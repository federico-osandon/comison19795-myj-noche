import React from 'react'
import ReactDOM from 'react-dom'
import App from './App'
import { getFirestoreApp } from './firebase/dbConfig'

import './index.css'
// import App from './App'

getFirestoreApp()

ReactDOM.render(  
   <App />
 ,
  document.getElementById('root')
)
