
import { BrowserRouter, Routes, Route} from 'react-router-dom'
import { CartContextProvider } from './context/cartContext'
import ItemListContainer from './components/ItemListContainer/ItemListContainer';
import ItemDetailContainer from './components/ItemDetailContainer/ItemDetailContainer';
import NavBar from './components/NavBar/NavBar';
import Cart from './components/Cart/Cart';



import './App.css'
import 'bootstrap/dist/css/bootstrap.min.css';



// ItemListContainer({greeting})
function App() { 

    const saludo= 'Hola soy ItemListContainer'
    console.log('app')

    return (
        <CartContextProvider>
            <div className='border border-3' >           
                    <BrowserRouter>
                        <NavBar/>
                        <Routes>               
                            <Route exact path='/' element={<ItemListContainer greeting={saludo} />} />
                            <Route exact path='/categoria/:idCategoria' element={<ItemListContainer greeting={saludo} />} />

                            <Route exact path='/detalle/:idDetalle' element={<ItemDetailContainer /> } />
                            <Route exact path='/cart' element={<Cart />} />
                        </Routes>
                    </BrowserRouter>                    
            </div>
        </CartContextProvider>
    )
}

export default App
