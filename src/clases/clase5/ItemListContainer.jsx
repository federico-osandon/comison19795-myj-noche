import { useState, useEffect } from 'react'

function ItemListContainer({greeting}) { 
    const [count, setCount] = useState(0) 
    const [fechaYHora, setFechaYHora] = useState(Date)
    const [ booleano, setBooleano] = useState(true)     

    
    const handleCount=()=>{
        //count = count+1// count++
        setCount(count+2)
        setFechaYHora(Date)
    }
    const handleBooleano=()=>{
        setBooleano(!booleano)
    }
    
    useEffect(()=>{// segundo plano, segunda instancia
        // accion llamada a api. 
        console.log('primer effecto')//demora mucho tiempo
    })

    useEffect(()=>{// segundo plano, segunda instancia
        // accion llamada a api. 
        console.log('llamada api 2, una sola vez')//demora mucho tiempo, para llamada a api.
    }, [])

    useEffect(() => {
        console.log('solo cuando cambie el estado de booleano')// addEventListener
        return () => {
            console.log('efecto limpieza')//removeEventListener
        }
        
    }, [booleano])



    // console.log(estado)
    console.log('componente montado, antes de retorno 3')

    return (
        <div>
            <h2>{greeting}</h2> 
            {count}  <br/> 
            {fechaYHora}  <br/> 
            <button onClick={handleCount}>+</button>       
            <button onClick={handleBooleano}>Booleano</button>       
        </div>
    )
}

export default ItemListContainer
