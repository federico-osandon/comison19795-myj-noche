import { Item } from "./Item"


function ComponentePadre({nombre, children}) {
    //console.log(props)
    return (
      <div>
        {nombre}
        {children}
      </div>
    )
  }
  export default ComponentePadre