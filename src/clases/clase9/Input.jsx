import {useState, useEffect} from 'react'
// import './input.css'


export const Input = () => {

    const inputHandler = (event)=>{
        event.preventDefault()
        //event.stopPropagation()
        console.log(event)
    }

    
    return (
        <div className="box" >
            <div className="border border-1 border-warning" >
                <input className="m-5" onKeyDown={ inputHandler } type="text" name="i" id="i"/>
            </div>
        </div>
    )
}
