export default function Select({ options, optionSelected, option=1 }) {

    return options.map((o) => (<> 
        
        <input 
            onChange= {(event)=>{
                optionSelected(o.value)
            }} 
            
            type='radio'
            name='color'
            checked={option===o.value}
            id={o.value}
            
        /> 
  
        <label for={o.value}>{o.text}</label>    
    </> ))  
  }
  