import {memo} from 'react'
import Item from '../Item/Item'


// memo(()=>{}) , memo(()={}, fn comparadora)

const ItemList = memo( ( {productos} )=> {
        console.log('itemList')
        return (
            <>
                { productos.map(prod => <Item prod={prod} />  ) }
            </>
        )
    }
, (oldProps, newProps)=> oldProps.productos.lenght === newProps.productos.lenght)

export default ItemList
