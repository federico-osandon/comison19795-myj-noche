import {Link, NavLink} from 'react-router-dom'

function Item({prod}) {
    console.log('item')
    return (
        
            <div 
                key={prod.id}
                className='col-md-4'
            >                        
                <div className="card w-100 mt-5" >
                    <div className="card-header">
                        {`${prod.title} - ${prod.categoria}`}
                    </div>
                    <div className="card-body">
                        <img src={prod.imageUrl} alt='' className='w-50' />
                        {prod.precio}                                                            
                    </div>
                    <div className="card-footer">
                        <Link to={`/detalle/${prod.id}`}>
                            <button className="btn btn-outline-primary btn-block">
                                detalle del producto
                            </button>                                              
                        </Link>                                            
                                                                                    
                    </div>
                </div>
            </div>
        
    )
}

export default Item
