import { useCartContext } from "../../../context/cartContext";

const ItemDetail = ({producto}) => {

    const {agregarAlCarrito} = useCartContext()
  

    function onAdd(cant) {
        console.log(cant);
        agregarAlCarrito( { ...producto, cantidad: cant  } )
    }
    
    return (
        <div>
            {producto.title} 
            <img src={producto.imageUrl} />
            {producto.precio}
           <button onClick={() => onAdd(2)}>Agregar al carrito</button> 
        </div>
    )
}

export default ItemDetail
