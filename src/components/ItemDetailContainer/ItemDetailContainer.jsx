import { doc, getDoc, getFirestore } from "firebase/firestore"
import { useState, useEffect } from "react"

import { useParams } from "react-router-dom"
import { getFetch } from '../../helpers/mock'
import ItemDetail from "./ItemDetail/ItemDetail"


const ItemDetailContainer = () => {
    const [producto, setProducto] = useState({})
    const {idDetalle} = useParams()
    
    useEffect(()=>{
        const db = getFirestore()
        const queryProd = doc(db, 'items', idDetalle)
        getDoc(queryProd)
        .then(resp => setProducto( {id: resp.id, ...resp.data()} ))
    }, [])

    console.dir(producto)

    return (
        <div>
            <ItemDetail producto={producto} />
        </div>
    )
}

export default ItemDetailContainer
