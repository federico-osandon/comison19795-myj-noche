// import '/NavBar.css'

// import { Navbar, Container, Nav } from "react-bootstrap"
import { Link } from 'react-router-dom'

import Navbar from "react-bootstrap/Navbar"
import Container from "react-bootstrap/Container"
import Nav from "react-bootstrap/Nav"
import NavDropdown from "react-bootstrap/NavDropdown"
import Widget from "./Widget"
import Titulo from "../../clases/clase5/Titulo"
import { useCartContext } from '../../context/cartContext'

function NavBar() {
    const {cantidadItem} = useCartContext()
    console.log('navbar')
    return (
        <>
            <Navbar collapseOnSelect expand="lg" bg="dark" variant="dark">
                <Container>
                <Link to='/'>React-Bootstrap</Link>
                <Navbar.Toggle aria-controls="responsive-navbar-nav" />
                <Navbar.Collapse id="responsive-navbar-nav">
                    <Nav className="me-auto">
                    <Link to="/categoria/remeras">Remeras</Link>
                    <Link to="/categoria/gorras">Gorras</Link>
                    
                    </Nav>
                    <Nav>
                    <Link to='/cart'>
                        <Widget />
                        {cantidadItem() !== 0 && cantidadItem() }
                    </Link>                    
                    </Nav>
                </Navbar.Collapse>
                </Container>
            </Navbar>
           
        </>
    )
}

export default NavBar
